class MessageDestroyJob < ApplicationJob
  queue_as :default

  def perform(message_id)
    ActionCable
      .server
      .broadcast 'room_channel', message: { type: 'destroy', id: message_id }
  end
end
