App.room = App.cable.subscriptions.create "RoomChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    message = data['message']
    switch message.type
      when 'render' then $('#messages').append message.message
      when 'destroy' then $(".message[data-id=#{message.id}]").remove()

$(document).on 'ajax:success', 'form.js-chat-form', (e) ->
  $(@)[0].reset()
