class MessagesController < ApplicationController
  def create
    Message.create! message_params
    head :no_content
  end

  def destroy
    Message.find(params[:id]).destroy!
  end

  protected

  def message_params
    params.require(:message).permit(:content)
  end
end
