class Message < ApplicationRecord
  after_create_commit { MessageBroadcastJob.perform_later self }
  after_destroy_commit { MessageDestroyJob.perform_later self.id }
end
